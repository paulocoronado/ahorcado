// https://www.web2generators.com/graphism-tools/css-sprite-creator


class Adivinador {


    constructor() {
        this.lista = ["PROGRAMACIÓN", "OBJETO", "CLASE"];
        this.seleccionarPalabra();
        this.crearContenedorPalabra();
        this.crearHTMLPalabra();

    }

    seleccionarPalabra() {
        let eleccion;
        eleccion = Math.round(Math.random(0, this.lista.length - 1));
        this.palabra = this.lista[eleccion];
    }

    crearContenedorPalabra() {

        let dimension = this.palabra.length;
        this.contenedorPalabra = new Array(dimension);
        this.contenedorPalabra.fill("");
    }

    crearHTMLPalabra() {

        let marcoLetra = document.getElementById("marcoLetra");
        let marcoPalabra = document.getElementById("marcoPalabra");

        //Cuadro para la letra
        let input = document.createElement("input");
        input.type = "text";
        input.name = "letraAdivinar";
        input.className = "txtLetraAdivinar";
        input.setAttribute("onkeyup", "miAdivinador.validarEntrada(this)");
        marcoLetra.appendChild(input);


        for (let i = 0; i < this.palabra.length; i++) {
            let input = document.createElement("input");
            input.type = "text";
            input.name = "letra" + i;
            input.className = "txtLetra";
            marcoPalabra.appendChild(input);
        }
    }

    validarEntrada(input) {
        console.log("Hola");

        input.value = input.value.substr(0, 1);
    }


}



function cambiarImagen(numero) {
    let elemento = document.getElementById("sprite");

    elemento.className = "sprite" + numero;


}